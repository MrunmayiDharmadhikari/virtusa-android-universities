package com.virtusa.universities.model

data class University(
    val title: String,
    val pageUrl: String,
    val imageUrl: String,
)
