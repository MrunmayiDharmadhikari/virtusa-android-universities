package com.virtusa.universities.repository

import com.google.gson.Gson
import com.virtusa.universities.model.University
import okhttp3.OkHttpClient
import okhttp3.Request
import java.util.Collections.emptyList

const val DATA_URL = "http://3.128.30.248:3000/data/universities"

class UniversityRepository {

    private val client = OkHttpClient()

    fun getUniversities(): List<University> {
        val request = Request.Builder().url(DATA_URL).build()
        val response = client.newCall(request).execute()
        if (response.isSuccessful && response.body != null) {
            val result = Gson().fromJson(response.body!!.string(), GetResponseDTO().javaClass)
            return parseUniversities(result)

        }

        throw IllegalStateException("Request failed")

    }

    private fun parseUniversities(responseDTO: GetResponseDTO) = toUniversityModels(responseDTO.res)

    private fun toUniversityModels(universities: List<UniversityDTO>) =
        universities.map { toUniversityModel(it) }

    private fun toUniversityModel(university: UniversityDTO) =
        University(university.name, university.web_pages[0], university.image_url)

}

data class GetResponseDTO(
    val status: String = "",
    val res: List<UniversityDTO> = emptyList()
)

data class UniversityDTO(
    val name: String = "",
    val image_url: String = "",
    val web_pages: List<String> = emptyList()
)