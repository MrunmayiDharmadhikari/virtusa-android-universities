package com.virtusa.universities.view

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.virtusa.universities.R
import com.virtusa.universities.model.University
import com.virtusa.universities.viewmodel.UniversityViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initUniversityListView()

    }

    private fun initUniversityListView() {


        val universities = mutableListOf<University>()
        val adapter = UniversityAdapter(this, universities)
        val viewModel = UniversityViewModel(universities, adapter)

        viewModel.getUniversityObservable()
            .subscribeOn(Schedulers.single())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(viewModel.getUniversityObserver())


        val recyclerView : RecyclerView = findViewById(R.id.universityListView)
        recyclerView.addOnScrollListener(ScrollListener(this))
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

    }

    class ScrollListener(context: Context) : RecyclerView.OnScrollListener() {
        private val toast = Toast.makeText(context, "Last", Toast.LENGTH_SHORT)

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if(recyclerView.hasReachedEnd()) {
                toast.cancel()
                toast.show()
            }
        }

        private fun RecyclerView.hasReachedEnd() = canScrollVertically(-1) &&
                !canScrollVertically(1) &&
                scrollState == RecyclerView.SCROLL_STATE_IDLE
    }

}
