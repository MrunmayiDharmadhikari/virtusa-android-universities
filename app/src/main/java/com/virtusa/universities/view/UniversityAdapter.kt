package com.virtusa.universities.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.virtusa.universities.R
import com.virtusa.universities.model.University

class UniversityAdapter(
    context: Context,
    private val universities: List<University>
) : RecyclerView.Adapter<UniversityAdapter.UniversityViewHolder>() {

    private val inflater = LayoutInflater.from(context)

    override fun getItemCount() = universities.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        UniversityViewHolder(inflater.inflate(R.layout.university_item_layout, parent, false))

    override fun onBindViewHolder(holder: UniversityViewHolder, position: Int) {
        val university = universities[position]
        holder.titleView.text = university.title
        holder.pageView.text = university.pageUrl
        Picasso.get().load(university.imageUrl).resize(100,100).into(holder.imageView)
    }


    class UniversityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleView : TextView = itemView.findViewById(R.id.universityTitle)
        val pageView : TextView = itemView.findViewById(R.id.universityPage)
        val imageView : ImageView = itemView.findViewById(R.id.universityImage)


    }
}
