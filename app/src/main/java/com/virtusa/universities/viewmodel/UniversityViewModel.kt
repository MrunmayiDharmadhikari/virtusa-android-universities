package com.virtusa.universities.viewmodel

import com.virtusa.universities.model.University
import com.virtusa.universities.repository.UniversityRepository
import com.virtusa.universities.view.UniversityAdapter
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleObserver
import io.reactivex.rxjava3.disposables.Disposable
import java.lang.Exception

class UniversityViewModel(val universities: MutableList<University>, val adapter: UniversityAdapter) {
    fun getUniversityObservable(): Single<List<University>> = Single.create {
        try {
            val universities = UniversityRepository().getUniversities()
            it.onSuccess(universities)
        } catch (e: Exception) {
            it.onError(e)
        }
    }

    fun getUniversityObserver() = object : SingleObserver<List<University>> {
        override fun onSuccess(result: List<University>?) {
            if (result != null) {
                universities.addAll(result)
                adapter.notifyDataSetChanged()
            }
        }

        override fun onError(e: Throwable?) {
            e?.printStackTrace()
        }

        override fun onSubscribe(d: Disposable?) {
            return
        }
    }
}